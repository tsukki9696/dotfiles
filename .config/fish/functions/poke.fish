function poke --wraps='pokemon-colorscripts -n' --description 'alias poke=pokemon-colorscripts -n'
  pokemon-colorscripts -n $argv
        
end
