function em --wraps='/usr/bin/emacs -nw' --description 'alias em=/usr/bin/emacs -nw'
  /usr/bin/emacs -nw $argv
        
end
