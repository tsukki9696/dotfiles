function yta-opus --wraps='youtube-dl --extract-audio --audio-format opus ' --description 'alias yta-opus=youtube-dl --extract-audio --audio-format opus '
  youtube-dl --extract-audio --audio-format opus  $argv
        
end
