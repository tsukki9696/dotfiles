function yta-vorbis --wraps='youtube-dl --extract-audio --audio-format vorbis ' --description 'alias yta-vorbis=youtube-dl --extract-audio --audio-format vorbis '
  youtube-dl --extract-audio --audio-format vorbis  $argv
        
end
