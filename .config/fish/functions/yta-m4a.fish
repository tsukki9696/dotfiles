function yta-m4a --wraps='youtube-dl --extract-audio --audio-format m4a ' --description 'alias yta-m4a=youtube-dl --extract-audio --audio-format m4a '
  youtube-dl --extract-audio --audio-format m4a  $argv
        
end
