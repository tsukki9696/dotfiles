function yta-best --wraps='youtube-dl --extract-audio --audio-format best ' --description 'alias yta-best=youtube-dl --extract-audio --audio-format best '
  youtube-dl --extract-audio --audio-format best  $argv
        
end
