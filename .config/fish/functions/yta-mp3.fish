function yta-mp3 --wraps='youtube-dl --extract-audio --audio-format mp3 ' --description 'alias yta-mp3=youtube-dl --extract-audio --audio-format mp3 '
  youtube-dl --extract-audio --audio-format mp3  $argv
        
end
