function yta-flac --wraps='youtube-dl --extract-audio --audio-format flac ' --description 'alias yta-flac=youtube-dl --extract-audio --audio-format flac '
  youtube-dl --extract-audio --audio-format flac  $argv
        
end
