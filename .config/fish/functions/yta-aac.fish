function yta-aac --wraps='youtube-dl --extract-audio --audio-format aac ' --description 'alias yta-aac=youtube-dl --extract-audio --audio-format aac '
  youtube-dl --extract-audio --audio-format aac  $argv
        
end
