# Dotfiles

![setup preview](https://gitlab.com/tsukki9696/dotfiles/-/raw/main/updated-rice.png)

## What is it?

My personal dotfiles repository. The purpose of this repository is to function as both a backup and as a way for people to freely inspect my configs. Some of the files here are for mostly, my personal use but anyone can freely clone this repository to use it as they see fit. Many configs here can change time to time, and what I use may change as well.

### My current programs
- **Window Manager**: bspwm
- **Bar**: Polybar
- **Terminal Emulator**: Alacritty with tmux
- **Text Editor**: Neovim(for coding), DOOM Emacs(for general writing) 
- **App Launcher**: Rofi 
- **File Explorer**: Thunar(GUI), Ranger(TUI)
- **Shell**: Fish with starship prompt

### Terminal scrips that I use
- **neofetch** for checking specs.
- **[shell-color-scripts](https://gitlab.com/dwt1/shell-color-scripts)** for getting a random ASCII when the terminal boots up.
- **[rickrollrc](https://github.com/keroserene/rickrollrc)** for comedic purposes.

## Questions

### How do I setup polybar?
`bash ~/.config/polybar/launch.sh --blocks` should work.

### Where can I find the wallpapers you use?
https://gitlab.com/tsukki9696/wallpapers contains everything that I use (none of the wallpapers are mine and I do not claim ownership of them).

